package net.oxmc.xcustomname;

import ru.pixelsky.db.Databases;
import ru.pixelsky.db.database.DatabaseStatement;
import ru.pixelsky.db.exceptions.DatabaseException;
import ru.pixelsky.db.query.QueryResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.logging.Logger;

/**
 * Работает с данными в БД
 */
public class DisplayNamesStorage {
    // Logger
    private Logger logger;

    // Display name by name
    private DatabaseStatement queryStatement;
    // Add display name
    private DatabaseStatement addStatement;
    // Update display name
    private DatabaseStatement updateStatement;
    // Table create statement
    private DatabaseStatement createStatement;

    public DisplayNamesStorage(Logger logger) throws IOException {
        addStatement =  new DatabaseStatement(Databases.getDefault(), "INSERT INTO `display_names`(`display_name`,`name`) VALUES(?, ?)");
        updateStatement = new DatabaseStatement(Databases.getDefault(), "UPDATE `display_names` SET `display_name`=? WHERE name = ?");
        queryStatement =  new DatabaseStatement(Databases.getDefault(), "SELECT `display_name` FROM `display_names` WHERE name = ?");
        createStatement =  new DatabaseStatement(Databases.getDefault(), readSQLFromResource("/sql/create.sql"));
    }

    /**
     * Create table if needed
     */
    public void init() throws DatabaseException {
        createStatement.executeUpdate();
    }

    /**
     * Retrieve display name for this player
     * @param playerName name of a player
     * @return player's display name, null if not specified
     */
    public String loadDisplayName(String playerName) throws DatabaseException {
        QueryResult result = queryStatement.executeQuery(playerName);
        if (!result.isEmpty()) {
            return result.getRow().getString(0);
        } else {
            return null;
        }
    }

    /**
     * Saves this display name
     */
    public void storeDisplayName(String playerName, String displayName) throws DatabaseException {
        if (loadDisplayName(playerName) == null)
            addStatement.executeUpdate(displayName, playerName);
        else
            updateStatement.executeUpdate(displayName, playerName);
    }

    /**
     * Достает инструкции SQL из файла
     */
    private String readSQLFromResource(String resource) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = getClass().getResourceAsStream(resource);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF8")));
            StringBuilder contents = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                contents.append(line);
                contents.append('\n');
            }
            return contents.toString();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }
}

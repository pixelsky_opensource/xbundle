package net.oxmc.xcustomname;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.pixelsky.db.exceptions.DatabaseException;

import java.util.Arrays;
import java.util.logging.Level;

public class XCNCommandHandler implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] params) {
        if (command.getName().equals("name")) {
            // Смена отображаемого ника у игрока, вызвавшего команду
            if (!sender.hasPermission("oxmc.customname.set.own"))
            {
                sender.sendMessage("У вас нет прав для этого!");
                return true;
            }
            if (!(sender instanceof Player))
            {
                return false;
            }
            Player player = (Player)sender;
            if (params.length == 0) {
                changeDisplayName(player.getName(), null);
                sender.sendMessage(ChatColor.GREEN + "Ваш ник над головой сброшен");
            } else {
                String newName = StringUtils.join(params, ' ');
                changeDisplayName(player.getName(), newName);
                sender.sendMessage(ChatColor.GREEN + "Ваш ник над головой изменен");
            }
            return true;
        } else if (command.getName().equals("setname")) {
            // Смена ника у игрока, введенного в команде
            if (!sender.hasPermission("oxmc.customname.set.other")) {
                sender.sendMessage("У вас нет прав для этого!");
                return true;
            }
            if (params.length >= 1) {
                OfflinePlayer p = Bukkit.getServer().getOfflinePlayer(params[0]);
                if (p != null) {
                    String newName = null;
                    if (params.length >= 2) {
                        newName = StringUtils.join(Arrays.copyOfRange(params, 1, params.length), ' ');
                    }
                    changeDisplayName(params[0], newName);

                    if (newName != null) {
                        sender.sendMessage(ChatColor.GREEN + "Ник над головой " + params[0] + " изменен");
                    } else {
                        sender.sendMessage(ChatColor.GREEN + "Ник над головой " + params[0] + " сброшен");
                    }
                } else {
                    sender.sendMessage(ChatColor.RED + "Такого игрока нет");
                }
                return true;
            }
        }

        return false;
    }

    private void changeDisplayName(String playerName, String displayName) {
        XCustomName.plugin.displayNameManager.setDisplayName(playerName, displayName);
        try {
            XCustomName.plugin.storage.storeDisplayName(playerName, displayName);
        } catch (DatabaseException e) {
            XCustomName.plugin.getLogger().log(Level.SEVERE, "Error storing display name for " + playerName, e);
        }
    }
}

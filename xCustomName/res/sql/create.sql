-- -----------------------------------------------------
-- Table `display_names`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `display_names` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `display_name` VARCHAR(45),
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci

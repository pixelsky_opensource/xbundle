package ru.pixelsky.db.database;

import org.apache.commons.dbcp.BasicDataSource;
import ru.pixelsky.db.exceptions.DatabaseConnectionException;
import ru.pixelsky.db.exceptions.DatabaseException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * For managing connections to concrete (username, password, url)
 */
public class Database {
    private Logger logger;
    private BasicDataSource dataSource;
    private ExecutorService taskExecutor = Executors.newFixedThreadPool(4);

    public Database(Logger logger, DatabaseParameters parameters) {
        init(logger, parameters);
    }

    /**
     * Called once on start
     */
    private void init(Logger logger, DatabaseParameters params) {
        this.logger = logger;
        this.dataSource = new BasicDataSource();
        dataSource.setUrl(params.getUrl());
        dataSource.setUsername(params.getUsername());
        dataSource.setPassword(params.getPassword());
        dataSource.setDriverClassName(params.getDriverClass());
    }

    /**
     * Returns active connection, you should close it as soon as you can
     * @return active connection to database
     * @throws ru.pixelsky.db.exceptions.DatabaseConnectionException if connection error occurred
     */
    public Connection getConnection() throws DatabaseConnectionException {
        if (dataSource == null)
            return null;
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new DatabaseConnectionException("Error creating connection", e);
        }
    }

    /**
     * Returns true if random connection is working properly, false otherwise
     */
    public boolean testConnection() {
        Connection con = null;
        try {
            con = getConnection();
            return con != null && con.isValid(10);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Error checking connection", e);
            return false;
        } catch (DatabaseConnectionException e) {
            logger.log(Level.SEVERE, "Error creating connection", e);
            return false;
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Error closing pool connection", e);
                }
            }
        }
    }

    /**
     * Closes connection pool
     */
    public void shutdown() throws DatabaseException {
        try {
            dataSource.close();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
        taskExecutor.shutdown();
    }

    public Logger getLogger() {
        return logger;
    }
}

package ru.pixelsky.db.exceptions;

public class DatabaseSQLException extends DatabaseException {
    public DatabaseSQLException() {
    }

    public DatabaseSQLException(String message) {
        super(message);
    }

    public DatabaseSQLException(String message, Throwable cause) {
        super(message, cause);
    }

    public DatabaseSQLException(Throwable cause) {
        super(cause);
    }

    public DatabaseSQLException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

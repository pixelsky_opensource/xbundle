package ru.pixelsky.db.executors;

public interface BukkitRunnable<T> {
    public void run(T param);
}

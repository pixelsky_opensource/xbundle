package ru.pixelsky.db;

import ru.pixelsky.db.database.Database;

public class Databases {
    private static Database defaultDatabase;

    public synchronized static void setDefaultDatabase(Database database) {
        if (defaultDatabase == null)
            defaultDatabase = database;
        else
            throw new IllegalStateException("Default data");
    }

    public static Database getDefault() {
        if (defaultDatabase == null) {
            synchronized (Databases.class) {
                // Synchronize variables
            }
        }
        return defaultDatabase;
    }
}

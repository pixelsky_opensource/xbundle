package ru.pixelsky.db.query;

import org.apache.commons.lang.Validate;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class QueryResult {

    private String[] columnNames;
    private List<Row> rows;

    public QueryResult() {
        this.rows = new ArrayList<Row>();
    }

    public Row getRow(int rowId) {
        if (rowId < 0 || rowId >= rows.size())
            throw new IllegalArgumentException("Invalid rowId: " + rowId + ", there are only " + rows.size() + " rows");
        return rows.get(rowId);
    }

    public boolean isEmpty() {
        return rows.isEmpty();
    }

    public Row getRow() {
        if (!isEmpty())
            return rows.get(0);
        return null;
    }

    public static QueryResult fromResultSet(ResultSet set) throws SQLException {
        QueryResult result = new QueryResult();
        List<Row> rows = new ArrayList<Row>();

        ResultSetMetaData metaData = set.getMetaData();
        int columnCount = metaData.getColumnCount();
        result.columnNames = new String[columnCount];
        for (int i = 0; i < metaData.getColumnCount(); i++) {
            result.columnNames[i] = metaData.getColumnName(i + 1);
        }

        while (set.next()) {
            Object[] values = new Object[columnCount];
            for (int i = 0; i < columnCount; i++) {
                Object obj = set.getObject(i + 1);
                Object rowObject = obj;
                if (obj instanceof java.sql.Blob) {
                    // Read blob
                    rowObject = LargeData.valueOf((Blob) obj);
                }
                values[i] = rowObject;
                rows.add(new Row(values));
            }
        }

        result.rows = rows;
        return result;
    }

    public int getColumnId(String columnName) {
        Validate.notNull(columnName);
        for (int i = 0; i < columnNames.length; i++)
            if (columnName.equals(columnNames[i]))
                return i;
        return -1;
    }

    public String getColumnName(int columnId) {
        if (columnId >= columnNames.length || columnId < 0)
            throw new IllegalArgumentException("columnId is " + columnId + ", must be between 0 and " + columnNames.length);
        return columnNames[columnId];
    }
}

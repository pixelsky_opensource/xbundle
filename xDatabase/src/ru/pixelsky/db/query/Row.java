package ru.pixelsky.db.query;

public class Row {
    private Object[] values;

    public Row(Object[] values) {
        this.values = values;
    }

    public int getInt(int columnId) {
        return (Integer)getObject(columnId);
    }

    public long getLong(int columnId) {
        return (Long)getObject(columnId);
    }

    public short getShort(int columnId) {
        return (Short)getObject(columnId);
    }

    public LargeData getLargeData(int columnId) {
        return (LargeData)getObject(columnId);
    }

    public String getString(int columnId) {
        return (String) getObject(columnId);
    }

    public Object getObject(int columntId) {
        validateColumnId(columntId);
        return values[columntId];
    }

    private void validateColumnId(int columnId) {
        if (values.length <= columnId || columnId < 0)
            throw new IllegalArgumentException("columnId is " + columnId + ", must be between 0 and " + values.length);
    }
}
